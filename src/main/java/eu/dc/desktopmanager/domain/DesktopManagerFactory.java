package eu.dc.desktopmanager.domain;

import eu.dc.desktopmanager.domain.DesktopManager;

import java.io.File;

/**
 * Created by me on 08.06.2016.
 */
public class DesktopManagerFactory {
    public static DesktopManager buildDesktopManager(String desktopPath, String olddesktopPath){
        File olddekstop = new File(olddesktopPath);
        File ignoreFile = new File(olddesktopPath+"/ignore.dm");
        File desktop = new File(desktopPath);
        return new DesktopManager(olddekstop, desktop,ignoreFile);
    }
}
