package eu.dc.desktopmanager.domain;

import eu.dc.desktopmanager.exception.DesktopManagerException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by me on 08.06.2016.
 */
public class DesktopManager {
    private final File desktop;
    private final File oldDesktop;
    private final File ignoreFile;

    public DesktopManager(File oldDesktop, File desktop, File ignoreFile) {
        this.oldDesktop = oldDesktop;
        this.desktop = desktop;
        this.ignoreFile = ignoreFile;
    }

    private void createFileIfNotExist(File f) throws IOException {
        if (!f.exists())
            f.createNewFile();
    }

    public void run() {
        try {
            oldDesktop.mkdirs();
            createFileIfNotExist(ignoreFile);
        } catch (IOException e) {
            throw new DesktopManagerException(e, "Couldn't create new files");
        }
        ArrayList<File> ignoredFiles = getIgnoredFiles();
        Date date = new Date();
        File saver = new File(oldDesktop.getAbsolutePath()+"/"+date.getDay()+"_"+date.getMonth()+"_"+date.getYear()+"#"+date.getHours()+"-"+date.getMinutes()+"-"+date.getSeconds());
        saver.mkdir();
        for(File f : desktop.listFiles()){
            if(!ignoredFiles.contains(f)){
                File fileDestination = new File(saver.getAbsolutePath()+"/"+f.getName());
                f.renameTo(fileDestination);
            }

        }
    }

    private ArrayList<File> getIgnoredFiles() {
        ArrayList<File> ignoredFiles = new ArrayList();
        try (BufferedReader br = new BufferedReader(new FileReader(ignoreFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                ignoredFiles.add(new File(line));
            }
        } catch (IOException e) {
            throw new DesktopManagerException(e, "Couldn't proceed ignore file");
        }
        return ignoredFiles;
    }
}
