package eu.dc.desktopmanager.exception;

/**
 * Created by me on 09.06.2016.
 */
public class DesktopManagerException extends RuntimeException {

    public DesktopManagerException(Exception e, String error){
        super(error, e);
    }

    public DesktopManagerException(String error){
        super(error);
    }
}
