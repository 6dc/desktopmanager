package eu.dc.desktopmanager;


import eu.dc.desktopmanager.domain.DesktopManager;
import eu.dc.desktopmanager.domain.DesktopManagerFactory;

/**
 * Created by me on 07.06.2016.
 */
public class DesktopManagerApplication {

    /**
     *
     * @param args (old desktop path)
     *
     */
    public static void main(String[] args){
        String desktopPath = System.getProperty("user.home") + "/Desktop";
        String odPath = desktopPath + "/Olddesktop";
        if(args.length > 0)
        {
            odPath=args[0];
        }
        DesktopManager manager = DesktopManagerFactory.buildDesktopManager(desktopPath, odPath);
        manager.run();
    }


}
